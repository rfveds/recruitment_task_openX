const { lengthOfLongestSubstring } = require('./main');

const data = [
    {
        input: 'abcabcbb',
        output: 3,
    },
    {
        input: 'bbbbb',
        output: 1,
    },
    {
        input: 'abcdefg',
        output: 7,
    },
    {
        input: '',
        output: 0,
    },
    {
        input: ' ',
        output: 1,
    },
    {
        input: 'aaab',
        output: 2,
    },
    {
        input: 'baa',
        output: 2,
    },
    {
        input: 'aacsba',
        output: 4,
    },
    {
        input: '@#$%^&*()_+',
        output: 11,
    },
    {
        input: 123,
        output: 'Input must be a string',
    },
];


describe.each(data)(`length Of Longest Substring`, (string) => {
    it(`that is ${string.input} should be ${string.output}`, () => {
        const output = lengthOfLongestSubstring(string.input);

        expect(output).toBe(string.output);
    });
});