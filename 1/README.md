## About

Test cases created with Jest JavaScript Testing Framework

## Instructions

In folder with this task run
```
npm install
```

To run tests run 
```
npm run test
```

## Test cases
| Input         |                                          Output Description |
|---------------|------------------------------------------------------------:|
| 'abcabcbb'    |                                                           3 |
| 'bbbbb'       |                     1; string with all repeating characters |
| 'abcdefg'     |                      1; string with no repeating characters |
| ''            |                                    0; test for empty string |
| ' '           |                                   1; string with whitespace |
| 'aaab'        |        2; string with repeating characters at the beginning |
| 'baa'         | 2; string with repeating characters at the beginning or end |
| 'aacsba'      |           4; string with repeating characters in the middle |
| '@#$%^&*()_+' |                          11; string with special characters |
| 123           |                    Input must be a string; check input type |