describe('SampleApp Login page', () => {

  it('should navigate to login page', () => {
    cy.visit('http://uitestingplayground.com/sampleapp')
  })

  it('should display an error message for empty user name', () => {
    cy.visit('http://uitestingplayground.com/sampleapp')
    cy.get('#loginstatus').should('contain', 'User logged out.')
    cy.get('input[name="Password"]').type('pwd')
    cy.get('button[id="login"]').click()
    cy.get('#loginstatus').should('contain', 'Invalid username/password')
  })

  it('should display an error message for  wrong password', () => {
    cy.visit('http://uitestingplayground.com/sampleapp')
    cy.get('#loginstatus').should('contain', 'User logged out.')
    cy.get('input[name="UserName"]').type('admin')
    cy.get('input[name="Password"]').type('invalid')
    cy.get('button[id="login"]').click()
    cy.get('#loginstatus').should('contain', 'Invalid username/password')
  })


  it('should display a message after successfull login', () => {
    cy.visit('http://uitestingplayground.com/sampleapp')
    cy.get('#loginstatus').should('contain', 'User logged out.')
    cy.get('input[name="UserName"]').type('admin')
    cy.get('input[name="Password"]').type('pwd')
    cy.get('button[id="login"]').click()
    cy.get('#loginstatus').should('contain', 'Welcome, admin!')
  })

})