## About

Test cases for successful and unsuccessful login, created with Cypress testing tool 

## Instructions

In folder with this task run
```
npm install
```

To open cypress and and then run tests
```
npx cypress open
```

Select e2e testing
